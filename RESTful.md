#RESTful Routing

#Introduction
* REST and WHY it matters

REST: REpresentational State Transfer
    * A mapping between HTTP routes and CRUD

* All 7 RESTful routes

Name        Path                HTTP verb       Purpose
-------------------------------------------------------------------------------------------------------
Index       /cars               GET             List all cars
New         /cars/new           GET             Show new car from 
Create      /cars               POST            Create new car then redirct somewhere
Show        /cars/:id           GET             Show info about one specific car
Edit        /cars/:id/edit      GET             Show edit form for one car
Update      /cars/:id           PUT             Update a particular car, then redirect somewhere
Destroy     /cars/:id           DELETE          Delete a particular car, then redirect somewhere

* Example of RESTful routing in pratice
* 


Blog

CREATE      
READ        /allBlogs
UPDATE      /updateBlog/:id
DESTROY     /destroyBlog/:id

#Putting the C in CRUD
* Add NEW route
* Add NEW template
* Add CREATE route
* Add CREATE template
* 

#SHOWtime 
* Add SHOW route
* Add SHOW template
* add links to show page
* Style show template
* 

#Edit/Update
* Add Edit Route
* Add Edit Form
* Add Update Route
* Add Update Form
* Ass Method-Override
* 

#DESTROY 
* Add Destroy Route
* Add Edit and Destroy links
* 
#Final Updates
* Sanitize blog body
* Style index
* Update REST table


<ol start="4">
  <li>Apples</li>
  <li>Oranges</li>
  <li>Pears</li>
</ol>