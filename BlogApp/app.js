var expressSanitizer    =   require("express-sanitizer"),
    methodOverride      =   require("method-override"),
    bodyParser          =   require("body-parser"),
    mongoose            =   require("mongoose"),
    express             =   require("express"),
    app                 =   express();
   
   
//APP CONFIG /Setting up mongoose to connect to db
mongoose.connect("mongodb://localhost:27017/restful_blog_app", {useNewUrlParser: true});
app.set('view engine', 'ejs');
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));
app.use(expressSanitizer()); // REQUIRED to go after bodyParser

//MONGOOSE /MODEL CONFIG
var blogSchema = new mongoose.Schema({
    
    title:      String,
    image:      String,
    body:       String,
    created:    {type: Date, default: Date.now}
    
});

//compiling into a model
var Blog = mongoose.model("Blogs", blogSchema); //taking the blogSchema (Properties of a campground) and compiled it to a model and save it to a variable called Blog. We can then use Blog to create new blogs, find blogs, remove blogs, update blogs ets...

// Blog.create({
//   title: "Test blog title",
//   image: "https://images.unsplash.com/photo-1542779283-429940ce8336?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80",
//   body:  "This is a test blog!"
// });



//RESTFUL ROUTES (7)

app.get("/", function(req, res){
   res.redirect("/blogs"); 
});

//INDEX ROUTE
app.get("/blogs", function(req, res){
  Blog.find({}, function(err, blogs){
      if(err){
          console.log("ERROR!");
      } else {
          
           res.render("index", {blogs: blogs});
      }
      
  }); 
});

//NEW ROUTE
app.get("/blogs/new", function(req, res){
    res.render("new"); 
});
//CREATE ROUTE
app.post("/blogs", function(req, res){
   //create blog
   console.log(req.body); //before sanitizing
   req.body.blog.body = req.sanitize(req.body.blog.body); //sanitizing
    console.log(req.body); //after sanitizing
   Blog.create(req.body.blog, function(err, newBlog){
       if(err){
           res.render("new");
       }else{
    //redirect to the index       
           res.redirect("/blogs");
       }
   });
});
//SHOW ROUTE
app.get("/blogs/:id", function(req, res){
    Blog.findById(req.params.id, function(err, foundBlog){
        if(err){
            res.redirect("/blogs");
        }else {
            res.render("show", {blog: foundBlog});
        }
    });
});
//EDIT ROUTE
app.get("/blogs/:id/edit", function(req, res){
   Blog.findById(req.params.id, function(err, foundBlog){
       if(err){
           res.render("/blogs");
       } else {
            res.render("edit", {blog: foundBlog}); 
       }
   });  
});
//UPDATE ROUTE
app.put("/blogs/:id", function(req, res){
    console.log(req.body); //before sanitizing
    req.body.blog.body = req.sanitize(req.body.blog.body); //sanitizing
    console.log(req.body); //after sanitizing
    Blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err, updatedBlog){ //(id, NewData, callback)
        if(err){
            res.redirect("/blogs");
        }else{
            res.redirect("/blogs/" + req.params.id);
        }
    });   
});

//DESTROY (delete) ROUTE
app.delete("/blogs/:id", function(req, res){
   //destroy blog
   Blog.findByIdAndRemove(req.params.id, function(err){
       if(err){
           res.rediect("/blogs");
       }else{
           res.redirect("blogs");
       }
   });
   
   //redirect somewhere
});

app.listen(process.env.PORT, process.env.IP, function(){
   console.log("BLOG SERVER HAS STARTED");
});   
    